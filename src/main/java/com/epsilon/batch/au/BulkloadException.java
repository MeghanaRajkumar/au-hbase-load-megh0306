package com.epsilon.batch.au;

public class BulkloadException extends Throwable {
    private static final long serialVersionUID = 1L;

    public BulkloadException(String message) {
        super(message);
    }
    public BulkloadException(String message, Exception exception) {
        super(message, exception);
    }
}
