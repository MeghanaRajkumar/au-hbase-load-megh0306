package com.epsilon.hbaseload.constants;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Constants {
    public static final Logger comLogger = LogManager.getLogger("com.epsilon.batch.au.SparkBulkLoader");
    public static final ObjectMapper objectMapper = new ObjectMapper();
    public static final String ADD_HASHING_INPUT_EXCP_MSG = "error while adding new column for hashing input ";
    public static final String EMPTY_INPUT_EXCP_MSG = "empty input ";
    public static final String COLUMNFAMILY_DATA = "data";
    public static final String COLUMNFAMILY_AUDIT = "audit";
    public static final String COLUMNFAMILY_AUDIT_COL1 = "created_date";
    public static final String COLUMNFAMILY_AUDIT_COL2 = "updated_date";
    public static final String INPUT_REQUIRED_MSG = "Invalid Input Argument; required: s3FilePath, hbaseTable and hBaseClusterConnectUrl";
}