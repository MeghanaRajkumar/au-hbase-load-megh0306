package com.epsilon.batch.au.model;

public class LambdaInputArgs {

    private String s3FilePath;
    private String hbaseTablename;
    private String hbaseClusterConnectUrl;

    private String entityColumnNames;

    private boolean testScope;

    public String getEntityColumnNames() {
        return entityColumnNames;
    }

    public void setEntityColumnNames(String entityColumnNames) {
        this.entityColumnNames = entityColumnNames;
    }


    public String getS3FilePath() {
        return s3FilePath;
    }

    public void setS3FilePath(String s3FilePath) {
        this.s3FilePath = s3FilePath;
    }

    public String getHbaseTablename() {
        return hbaseTablename;
    }

    public void setHbaseTablename(String hbaseTablename) {
        this.hbaseTablename = hbaseTablename;
    }

    public String getHbaseClusterConnectUrl() {
        return hbaseClusterConnectUrl;
    }

    public void setHbaseClusterConnectUrl(String hbaseClusterConnectUrl) {
        this.hbaseClusterConnectUrl = hbaseClusterConnectUrl;
    }

    public boolean isTestScope() {
        return testScope;
    }

    public void setTestScope(boolean testScope) {
        this.testScope = testScope;
    }

    public static final String KEYING_INPUT_REQUIRED_MSG = "Invalid Input Argument; required: s3FilePath, hbaseTableName";
}
