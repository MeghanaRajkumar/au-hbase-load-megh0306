package com.epsilon.batch.au.util;

import com.epsilon.batch.au.model.LambdaInputArgs;
import org.apache.hadoop.hbase.client.Result;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.valueOf;
import static java.util.Collections.sort;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.apache.commons.collections.MapUtils.isEmpty;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;


public class RowKeyGenerator {

    private static final String SEPARATOR = "_";

    private RowKeyGenerator() {}

    public static String generateRowKeyPrefixFor(SparkSession spark, LambdaInputArgs inputArgs, final Map userDefinedKeys) {
        if (isEmpty(userDefinedKeys)) {
            return null;
        }
        final List<String> keys = newArrayList(userDefinedKeys.keySet());
        sort(keys);
        final StringBuilder userPrimaryKeys = new StringBuilder();
        keys.forEach(key -> userPrimaryKeys.append(userDefinedKeys.get(key)).append(SEPARATOR));
        userPrimaryKeys.deleteCharAt(userPrimaryKeys.lastIndexOf(SEPARATOR));
        String key = sha256Hex(userPrimaryKeys.toString());
        return key;
    }


    static Boolean verifyAndConfirm(final String dataColumnFamily, final Result result, final Map primaryKeys) {
        if (result == null || result.isEmpty()) {
            return true;
        }
        final NavigableMap<byte[], byte[]> familyMap = result.getFamilyMap(toBytes(dataColumnFamily));
        for (Map.Entry<byte[], byte[]> entry : familyMap.entrySet()) {
            final String key = new String(entry.getKey());
            final String value = new String(entry.getValue());
            if (primaryKeys.containsKey(key) && !valueOf(primaryKeys.get(key)).equals(value)) {
                return false;
            }
        }
        return true;
    }
}
