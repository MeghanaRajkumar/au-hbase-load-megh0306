package com.epsilon.batch.au.util;

import com.epsilon.hbaseload.constants.Constants;
import com.epsilon.batch.au.model.LambdaInputArgs;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.exceptions.HBaseException;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class BulkUtil {


    private static final String HEADER = "header";

    public static LambdaInputArgs decodeInputArgs(String driverInputArgs) throws IOException {
        String decodedString = new String(Base64.getDecoder().decode(driverInputArgs));
        LambdaInputArgs parsedInputArgs = Constants.objectMapper
                .readValue(decodedString, LambdaInputArgs.class);
        if (StringUtils.isBlank(parsedInputArgs.getS3FilePath())
                || StringUtils.isBlank(parsedInputArgs.getHbaseClusterConnectUrl()) ||  StringUtils.isBlank(parsedInputArgs.getHbaseTablename())) {
            throw new IllegalArgumentException(Constants.INPUT_REQUIRED_MSG);
        }
        return parsedInputArgs;
    }


    public static Dataset<Row> readDataFileWithHeader(SparkSession spark, LambdaInputArgs inputArgs) {
        System.out.println("S3 File Path...:"+inputArgs.getS3FilePath());
        return spark.read().format("csv").option(HEADER, "true").option("delimiter", ",")
                .load(inputArgs.getS3FilePath());

    }

    public static Map<Dataset<Row>,Dataset<Row>> getDataSetForNewKeyAssignment(Dataset<Row> incomingFileDS,
                                                                               Dataset<Row> hBaseDataSet) {
        Map<Dataset<Row>,Dataset<Row>> datasetMap = new HashMap<>();
        datasetMap.put(hBaseDataSet,incomingFileDS);
        return datasetMap;
    }



}
