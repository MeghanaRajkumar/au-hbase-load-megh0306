package com.epsilon.batch.au;

import com.epsilon.batch.au.model.LambdaInputArgs;
import com.epsilon.batch.au.util.BulkUtil;
import com.epsilon.batch.au.util.RowKeyGenerator;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.mapreduce.TableOutputFormat;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.mapreduce.Job;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.*;
import org.apache.spark.SparkConf;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.spark.sql.Row;
import com.epsilon.hbaseload.constants.Constants;
import scala.Tuple2;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.Arrays;
import java.util.Map;



/**
 * (c) geekmj.org All right reserved
 * <p>
 * Main Spark Driver Program Class
 * It creates Resilient Distributed Dataset for a log file (external data source)
 * It does following operation on RDD
 * 1. Total no. of lines in log file
 * 2. Total characters in log file
 * 3. Total no. of URL in log file with HTML and GIF extension
 */
public class SparkBulkLoader {
  public static int count=0;
  public static void main(String[] driverArgs) throws Exception {
    /* Define Spark Configuration */
    LambdaInputArgs inputArgs = BulkUtil.decodeInputArgs(driverArgs[0]);

    Constants.comLogger.info(" ---> Input Arguments - {}", inputArgs);

    final String HEADER = "header";

    String appName = "BulkLoaderDriver";
    // Spark Config
    SparkConf conf = new SparkConf().setAppName(appName);
    JavaSparkContext sc = new JavaSparkContext(conf);

    SparkSession.Builder sparkBuilder = SparkSession.builder().appName(appName).config("spark.hadoop.validateOutputSpecs", false).enableHiveSupport();
    SparkSession spark = sparkBuilder.getOrCreate();

    //Read File Contents
    Dataset<Row> incomingFileDS = BulkUtil.readDataFileWithHeader(spark, inputArgs);
    JavaRDD<Row> rowJavaRDD = incomingFileDS.toJavaRDD();
    Dataset<Row> hBaseDataSet = null;
    Map<Dataset<Row>,Dataset<Row>> needNewkeysDs = BulkUtil.getDataSetForNewKeyAssignment(incomingFileDS, hBaseDataSet);
    //fetching dynamic entity column names from lambda
    String[] entityHeaders = inputArgs.getEntityColumnNames().split(",");
    String RowKeyValue = RowKeyGenerator.generateRowKeyPrefixFor(spark,inputArgs,needNewkeysDs);
    String[] columnNames = incomingFileDS.columns();
    String[] colnames = columnNames;
    Arrays.sort(colnames);
    Arrays.sort(entityHeaders);
    boolean result =Arrays.equals(colnames,entityHeaders);
    if (result== false)
    {
      System.out.println("ERROR: The Dynamic entity column headers doesn't match with the csv file. Can't bulkload into Hbase Table.");
      System.out.println("exit...");
      System.exit(0);
    }
    sparkHbaseBulkInsert(inputArgs, rowJavaRDD, Constants.COLUMNFAMILY_DATA, columnNames, RowKeyValue, incomingFileDS);

    sc.stop();
    sc.close();
  }
public static int rowkeycounter()
{
  return ++count;
}
  public static boolean sparkHbaseBulkInsert(LambdaInputArgs inputArgs,
                                             JavaRDD<Row> rowJavaRDD, final String columnFamily, final String[] columnNames, String Rowkey,  Dataset<Row> incomingFileDS)
          throws Exception {
    Boolean retSucess = true;
    long length= incomingFileDS.count();
    Configuration config = getHbaseConfig(inputArgs);

    try {

      Job newApiJobConfig = Job.getInstance(config);
      newApiJobConfig.getConfiguration().set(TableOutputFormat.OUTPUT_TABLE, inputArgs.getHbaseTablename());
      newApiJobConfig.setOutputFormatClass(TableOutputFormat.class);
      newApiJobConfig.setMapOutputKeyClass(String.class);

      /*HBaseAdmin ad = new HBaseAdmin(config); // Instantiate HBaseAdmin class.
      HColumnDescriptor cDescriptor = new HColumnDescriptor(Constants.COLUMNFAMILY_AUDIT); //Instantiate columnDescriptor
      ad.addColumn(inputArgs.getHbaseTablename(), cDescriptor);*/


      JavaPairRDD<ImmutableBytesWritable, Put> writeToHbase = rowJavaRDD.mapToPair(new PairFunction<Row, ImmutableBytesWritable, Put>() {
        @Override
        public Tuple2<ImmutableBytesWritable, Put> call(Row row)
                throws Exception {
          // Add Key
          int counter = rowkeycounter();
            String key= (Rowkey+"_"+counter);


          System.out.println("Row Key..." + key);
          Constants.comLogger.info(" ---> Row Key - {}", key);
          Put put = new Put(Bytes.toBytes(key));
          // Add Value
          for (int i = 0; i < columnNames.length; i++) {
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnNames[i]),
                    Bytes.toBytes(row.getString(i)));
          }

          //Put audit Columns
          String datenow = LocalDateTime.now().format(DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss"));
          System.out.println("Formatting the date.."+datenow);
          put.addColumn(Bytes.toBytes(Constants.COLUMNFAMILY_AUDIT),Bytes.toBytes(Constants.COLUMNFAMILY_AUDIT_COL1),
                  Bytes.toBytes(datenow));
          put.addColumn(Bytes.toBytes(Constants.COLUMNFAMILY_AUDIT),Bytes.toBytes(Constants.COLUMNFAMILY_AUDIT_COL2),
                  Bytes.toBytes(datenow));
          return new Tuple2<ImmutableBytesWritable, Put>(new ImmutableBytesWritable(), put);
        }
      });


      // Write to Hbase

    writeToHbase.saveAsNewAPIHadoopDataset(newApiJobConfig.getConfiguration());


    } catch (Exception e) {
      Constants.comLogger.error("Error while writing data to hbase", e.getMessage());
    }
    return retSucess;
  }


  private static Configuration getHbaseConfig(LambdaInputArgs inputArgs) {
    Configuration config = null;
    try {
      config = HBaseConfiguration.create();
      config.set("hbase.zookeeper.quorum", inputArgs.getHbaseClusterConnectUrl());
      config.set("hbase.zookeeper.property.clientPort", "2181");
      config.set(TableInputFormat.INPUT_TABLE, inputArgs.getHbaseTablename());
      HBaseAdmin.checkHBaseAvailable(config);
      System.out.println("HBase is running!");
    } catch (MasterNotRunningException e) {
      //logger.error("HBase is NOT running!");
      System.out.println("HBase is not running!");
      System.exit(1);
    } catch (Exception ce) {
      ce.printStackTrace();
    }
    return config;
  }
}